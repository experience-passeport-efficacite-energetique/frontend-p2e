import { mountWithDefaults } from '@test/helpers/helpers.js'
import Login from '@/public/Login.vue'
import expect from 'expect'

describe('Login.vue', () => {
  const wrapper = mountWithDefaults(Login)
  it('Page renders email field', () => {
    expect(wrapper.find('input[type="email"]').exists()).toBe(true)
  })
  it('Page renders password field', () => {
    expect(wrapper.find('input[type="password"]').exists()).toBe(true)
  })
  it('Page page should not render profile and logout buttons', () => {
    expect(wrapper.findAll('.q-toolbar button').length).toBe(0)
  })
})
