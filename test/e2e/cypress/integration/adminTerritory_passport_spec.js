describe('Admin territory - passport', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
          { name: 'adminterritory2', data: this.users.adminterritory2, creds: this.creds.adminterritory2 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor1,
        items: [
          { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' },
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'adminterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.adminterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.adminterritory1)
    cy.getCreatedItemByName('passport1').then(passport => {
      cy.visit('/#/admin/passport?passport=' + passport.id)
    })

    cy.wait('@getVentilations')
    cy.wait('@getHeatings')
    cy.wait('@getDHW')
    cy.wait('@getPassport')
    cy.contains('Données générales')
    cy.get('[data-cy=passport-address] :input').should('have.value',
      this.users.particulier1.address.address
      + ', ' + this.users.particulier1.address.zipcode
      + ' ' + this.users.particulier1.address.city
    )
    cy.get('[data-cy=passport-floors]').contains('Maison sur 2 niveaux dont combles aménagés')
    cy.contains('Murs extérieurs')
    cy.contains('Plancher bas')
    cy.contains('Toiture')
    cy.contains('Ouvrants')
    cy.contains('Ventilation')
    cy.contains('Étanchéité à l\'air')
    cy.contains('Gestion dynamique du chauffage')
    cy.contains('Eau Chaude Sanitaire')
    cy.contains('Dossier de diagnostic technique (DDT)')

    cy.get('[data-cy="passport-btn-submit"]').should('not.exist')
    cy.get('[data-cy="passport-btn-cancel"]').should('not.exist')
    cy.get('[data-cy="passport-btn-updateSynthesis"]').should('not.exist')
    cy.get('[data-cy="passport-btn-createSynthesis"]').should('not.exist')

  })
})
