describe('Admin - User management', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Create and activate auditor as admin', function () {
    cy.log("## NO DATA TO INITIALIZE - BEGINNING TESTS")
    cy.login(this.creds.admin)
    cy.visit('/#/admin/user')
    let timestamp = Date.now()
    cy.get('[data-cy="user-firstname"] :input').type(this.users.auditorGenerated.firstname)
    cy.get('[data-cy="user-lastname"] :input').type(this.users.auditorGenerated.lastname)
    cy.get('[data-cy="user-email"] :input').type(this.users.auditorGenerated.email.replace('{%timestamp}', timestamp))
    cy.get('[data-cy="user-phone"] :input').type(this.users.auditorGenerated.phone)
    cy.get('[data-cy="user-address"] :input').type(this.users.auditorGenerated.address)
    cy.get('[data-cy="user-roles"]').contains('Particulier').click()
    cy.get('.q-item-label').contains('Auditeur').click()
    cy.get('.q-item-label').contains('Particulier').click()

    cy.get('[data-cy="user-submit"]').click()
    cy.wait('@createUser').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    cy.getMailtrapInbox().then(response => {
      let messageId = response.body[0].id
      expect(response.body[0].to_email).equals(this.users.auditorGenerated.email.replace('{%timestamp}', timestamp))
      cy.getMailtrapMessage(response.body[0].html_path).then(response => {
        cy.activateAccountFromMail(response.body).then(response => {
          cy.deleteMailtrapMessage(messageId)
          expect(response.status).equals(200)
        })
      })

    })
  })

})
