describe('Admin - vigilance points', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('vigilancePoints').as('vigilancePoints')
    cy.fixture('vigilancePointsConditions').as('vigilancePointsConditions')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test vigilance points list display as admin', function () {
    var itemsToCreate = [
      {
        type: 'vigilancePoints',
        items: [
          { name: 'vigilancePoint1', data: this.vigilancePoints.vigilancePoint1 },
          { name: 'vigilancePoint2', data: this.vigilancePoints.vigilancePoint2 }
        ]
      },
      {
        type: 'vigilancePointsCondition',
        items: [
          { name: 'vigilancePointCondition1', data: this.vigilancePointsConditions.vigilancePointCondition1, vpName: 'vigilancePoint1' },
          { name: 'vigilancePointCondition2', data: this.vigilancePointsConditions.vigilancePointCondition2, vpName: 'vigilancePoint1' },
          { name: 'vigilancePointCondition3', data: this.vigilancePointsConditions.vigilancePointCondition3, vpName: 'vigilancePoint2' },
          { name: 'vigilancePointCondition4', data: this.vigilancePointsConditions.vigilancePointCondition4, vpName: 'vigilancePoint2'}
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)


    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.admin)

    cy.visit('/#/admin/vigilancePoints')
    cy.wait('@getVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.get('tr').should('have.length', 3)

    cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
      cy.get('[data-cy=vigilancesPoints-edit-id-' + vigilancePoint.id + ']').click({ force: true })
    })

    cy.wait('@getVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.visit('/#/admin/vigilancePoints')
    cy.wait('@getVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    // Create Vigilance Point
    cy.get('[data-cy="vigilancesPoints-create"]').click()
    cy.get('[data-cy="vigilancePoint-title"] :input').type(this.vigilancePoints.vigilancePoint3.title)
    cy.get('[data-cy="vigilancePoint-description"] :input').type(this.vigilancePoints.vigilancePoint3.description)
    cy.get('[data-cy="vigilancePoint-complexity"] :input').type(this.vigilancePoints.vigilancePoint3.complexity)
    cy.get('[data-cy="vigilancePoint-priority"] :input').type(this.vigilancePoints.vigilancePoint3.priority)
    cy.get('[data-cy="vigilancePoint-submit"]').click()
    cy.wait('@createVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    // Ajout condition + test
    cy.get('[data-cy="vigilancePoint-addCondition"]').click()
    cy.get('[data-cy="vigilancePointCondition-precedingLot"]').click({ force: true })
    cy.get(':nth-child(2) > .q-item-main').click({ force: true })
    cy.get('[data-cy="vigilancePointCondition-precedingLotValueBefore"]').click({ force: true })
    cy.get(':nth-child(1) > .q-item-main').click({ force: true })
    cy.get('[data-cy="vigilancePointCondition-followingLot"]').click({ force: true })
    cy.get(':nth-child(3) > .q-item-main').click({ force: true })
    cy.get('[data-cy="vigilancePointCondition-followingLotValueBefore"]').click({ force: true })
    cy.get(':nth-child(1) > .q-item-main').click({ force: true })
    cy.get('[data-cy="vigilancePoint-addCondition-submit"]').click({ force: true })
    cy.get('[data-cy="vigilancePoint-submit"]').click({ force: true })
    cy.wait('@updateVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.get('tr').should('have.length', 2)

    cy.visit('/#/admin/vigilancePoints')
    cy.wait('@getVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.get('tr').should('have.length', 4)

    //delete VP2 + test
    cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
      cy.get('[data-cy=vigilancesPoints-delete-id-' + vigilancePoint.id + ']').click({ force: true })
    })
    cy.contains('Supprimer le point de vigilance').click({ force: true })
    cy.wait('@deleteVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.get('tr').should('have.length', 3)

    //edit condition + test
    cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
      cy.get('[data-cy=vigilancesPoints-edit-id-' + vigilancePoint.id + ']').click({ force: true })
    })

    cy.wait('@getVigilancePoint').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.getCreatedItemByName('vigilancePointCondition1').then(vigilancePointsCondition => {
      cy.get('[data-cy=vigilancesPointCondition-edit-id-' + vigilancePointsCondition.id + ']').click({ force: true })
    })

    cy.get('[data-cy="vigilancePointCondition-precedingLot"]').click({ force: true })
    cy.get(':nth-child(3) > .q-item-main').click({ force: true })
    cy.get('[data-cy="vigilancePoint-addCondition-submit"]').click()
    cy.wait('@updateVigilancePointCondition').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.get('tr').should('have.length', 3)


    //delete condition + test
    cy.getCreatedItemByName('vigilancePointCondition1').then(vigilancePointsCondition => {
      cy.get('[data-cy=vigilancesPointCondition-delete-id-' + vigilancePointsCondition.id + ']').click({ force: true })
    })

     cy.wait('@deleteVigilancePointCondition').then(function (xhr) {
       expect(xhr.status).equals(200)
     })

     cy.get('tr').should('have.length', 2)


  })
})
