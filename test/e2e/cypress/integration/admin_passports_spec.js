describe('Admin - passports', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passports list display as admin', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'auditor2', data: this.users.auditor2, creds: this.creds.auditor2 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 },
          { name: 'lead2', data: this.leads.lead2 },
          { name: 'lead3', data: this.leads.lead3 }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor1,
        items: [
          { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' },
          { name: 'passport2', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead2' }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor2,
        items: [
          { name: 'passport3', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead3' }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.admin)
    cy.visit('/#/admin/passports')

    cy.contains('tr', 'Propriétaire').should('be.visible')
    cy.contains('tr', 'Adresse').should('be.visible')
    cy.contains('tr', 'Code postal').should('be.visible')
    cy.contains('tr', 'Ville').should('be.visible')
    cy.contains('tr', 'Créé le').should('be.visible')
    cy.contains('tr', 'Modifié le').should('be.visible')
    cy.contains('tr', 'Auditeur').should('be.visible')
    cy.contains('tr', 'Action').should('be.visible')

    cy.get('[data-cy="passports-search"] :input')
      .clear({ force: true })
      .type(this.users.auditor1.firstname + ' ' + this.users.auditor1.lastname, { force: true })
    cy.wait(250) //wait 250ms for quasar table to update with filter params
    cy.get('tr').contains(this.users.auditor1.firstname + " " + this.users.auditor1.lastname).should('be.visible')
    cy.get('tr').contains(this.users.particulier1.firstname + " " + this.users.particulier1.lastname).should('be.visible')
    cy.get('tr').should('have.length', 3)

    cy.get('[data-cy="passports-search"] :input')
      .clear({ force: true })
      .type(this.users.auditor2.firstname + ' ' + this.users.auditor2.lastname, { force: true })
    cy.wait(250) //wait 250ms for quasar table to update with filter params
    cy.get('tr').contains(this.users.auditor2.firstname + " " + this.users.auditor2.lastname).should('be.visible')
    cy.get('tr').contains(this.users.particulier1.firstname + " " + this.users.particulier1.lastname).should('be.visible')
    cy.get('tr').should('have.length', 2)

    cy.get('[data-cy="passports-search"] :input').clear({ force: true })
    cy.wait(250) //wait 250ms for quasar table to update with filter params
    cy.getCreatedItemByName('passport1').then(passport => {
      cy.get('[data-cy="passports-edit-id-' + passport.id + '"]').click()
    })
    cy.wait('@getVentilations')
    cy.wait('@getHeatings')
    cy.wait('@getDHW')
    cy.wait('@getPassport')
    cy.contains('Données générales')
    cy.get('[data-cy=passport-address] :input').should('have.value',
      this.users.particulier1.address.address
      + ', ' + this.users.particulier1.address.zipcode
      + ' ' + this.users.particulier1.address.city
    )
    cy.contains('Murs extérieurs')
    cy.contains('Plancher bas')
    cy.contains('Toiture')
    cy.contains('Ouvrants')
    cy.contains('Ventilation')
    cy.contains('Étanchéité à l\'air')
    cy.contains('Gestion dynamique du chauffage')
    cy.contains('Eau Chaude Sanitaire')
    cy.contains('Dossier de diagnostic technique (DDT)')
  })
})
