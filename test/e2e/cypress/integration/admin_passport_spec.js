describe('Admin - passport', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport edit as admin', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor1,
        items: [
          { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName('passport1').then(passport => {
      cy.visit('/#/admin/passport?passport=' + passport.id)
    })

    cy.wait('@getVentilations')
    cy.wait('@getHeatings')
    cy.wait('@getDHW')
    cy.wait('@getPassport')
    cy.contains('Données générales')
    cy.get('[data-cy=passport-address] :input').should('have.value',
      this.users.particulier1.address.address
      + ', ' + this.users.particulier1.address.zipcode
      + ' ' + this.users.particulier1.address.city
    )
    cy.get('[data-cy=passport-floors]').contains('Maison sur 2 niveaux dont combles aménagés')
    cy.contains('Murs extérieurs')
    cy.contains('Plancher bas')
    cy.contains('Toiture')
    cy.contains('Ouvrants')
    cy.contains('Ventilation')
    cy.contains('Étanchéité à l\'air')
    cy.contains('Gestion dynamique du chauffage')
    cy.contains('Eau Chaude Sanitaire')
    cy.contains('Dossier de diagnostic technique (DDT)')

    cy.get('[data-cy=passport-address] :input')
      .clear({ force: true })
      .type("10 rue Gras", { force: true })
    cy.wait('@searchAddress')
    cy.get(':nth-child(1) > .q-item-main').click()

    cy.get('[data-cy=passport-roofData-attictype]').click()
    cy.get(':nth-child(3) > .q-item-main').click()

    cy.get('[data-cy=passport-roofData-atticTypePlanned]').contains('Oui').click()

    cy.get('[data-cy=passport-floorData-type]').contains('Autre local non chauffé').click()

    cy.get('[data-cy=passport-btn-submit]').click()
    cy.wait('@updatePassport').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
  })
})
