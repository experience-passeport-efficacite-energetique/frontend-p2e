describe('Admin territory - leads', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
          { name: 'adminterritory2', data: this.users.adminterritory2, creds: this.creds.adminterritory2 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.leadPantin }
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Assigning leads")
    cy.login(this.creds.auditor1)
    cy.getCreatedItemByName('lead1').then(lead => {
      cy.assignLead(lead)
    })

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'adminterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.adminterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.adminterritory2)
    cy.visit('/#/admin/leads')
    cy.contains('Aucune inscription')
    cy.get('tr').should('have.length', 1)
    cy.logout()

    cy.visit('/#/login')
    cy.get('[data-cy="login-username"] :input').type(this.creds.adminterritory1.username)
    cy.get('[data-cy="login-password"] :input').type(this.creds.adminterritory1.password)
    cy.get('[data-cy="login-submit"]').click()
    cy.wait('@auth').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.visit('/#/admin/leads')
    cy.wait('@getLead').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.contains('Pantin')
    cy.get('tr').should('have.length', 2)

    cy.getCreatedItemByName('lead1').then(lead => {
      cy.get('[data-cy=leads-lead-id-' + lead.id + ']').click({ force: true })
    })

    cy.wait('@getLead').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

  })
})
