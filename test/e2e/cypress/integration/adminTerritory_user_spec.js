describe('Admin territory - users', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'userterritory1', data: this.users.userterritory1, creds: this.creds.userterritory1 }
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'adminterritory1', 'userterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.adminterritory1)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.userterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.adminterritory1)
    cy.getCreatedItemByName('userterritory1').then(user => {
      cy.visit('/#/admin/user?user=' + user.id)
    })

    cy.wait('@getUser').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.contains('Utilisateur territorial')
    cy.get('[data-cy="user-lastname"] :input').clear({ force: true }).type("whatev", { force: true })

    cy.get('[data-cy="user-submit"]').click({ force: true })

    cy.wait('@updateUser', { requestTimeout: 10000 }).then(function (xhr) {
      expect(xhr.status).equals(200)
    })

  })
})
