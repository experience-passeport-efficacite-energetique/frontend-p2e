
describe('Home page', function () {
  it('Visits the home page', function () {
    cy.visit('/')

    cy.contains('Connectez vous pour accéder à l\'application').should('be.visible')
    cy.contains('Connexion').should('be.visible')
    cy.contains('Mot de passe oublié?').should('be.visible')
    cy.contains('Créer mon compte').should('be.visible')
    cy.fixture('creds').then(creds => {
      cy.get('[data-cy="login-username"] :input').type(creds.admin.username)
      cy.get('[data-cy="login-password"] :input').type(creds.admin.password)
      cy.get('[data-cy="login-submit"]').click()
      cy.wait(5000) // Wait dashboard load
      cy.contains(creds.admin.username).should('be.visible')
    })

  })

  it('User can register', function () {
    cy.visit('/')
    cy.contains('Créer mon compte').click()
    cy.fixture('profile').then((user) => {
      cy.get('[data-cy="register-firstname"] :input').type(user.firstname)
      cy.get('[data-cy="register-lastname"] :input').type(user.lastname)
      cy.get('[data-cy="register-email"] :input').type(user.email)
      cy.get('[data-cy="register-phone"] :input').type(user.phone)
      cy.get('[data-cy="register-password"] :input').type(user.password)
      cy.get('[data-cy="register-passwordConfirm"] :input').type(user.password, { force: true })
    })
  })
})
