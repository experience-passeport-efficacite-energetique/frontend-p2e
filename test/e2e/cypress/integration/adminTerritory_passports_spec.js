describe('Admin territory - passports', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
          { name: 'adminterritory2', data: this.users.adminterritory2, creds: this.creds.adminterritory2 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor1,
        items: [
          { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' },
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Defining test territory")
    cy.login(this.creds.admin)
    cy.getCreatedItemByName(['territory1', 'adminterritory1']).then(createdItems => {
      cy.assignCityToTerritory(createdItems.territory1, 35357)
      cy.assignUserToTerritory(createdItems.territory1, createdItems.adminterritory1)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.adminterritory2)
    cy.visit('/#/admin/passports')
    cy.contains('Aucun passeport')
    cy.get('tr').should('have.length', 1)

    cy.login(this.creds.adminterritory1)
    cy.reload(true)
    cy.contains('Pantin')
    cy.get('tr').should('have.length', 2)

    cy.getCreatedItemByName('passport1').then(passport => {
      cy.get('[data-cy=passports-edit-id-' + passport.id + ']').click({ force: true })
    })

    cy.wait('@getPassport').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

  })
})
