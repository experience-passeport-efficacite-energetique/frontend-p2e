describe('Admin - territory', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'adminterritory1', data: this.users.adminterritory1, creds: this.creds.adminterritory1 },
          { name: 'adminterritory2', data: this.users.adminterritory2, creds: this.creds.adminterritory2 },
          { name: 'userterritory1', data: this.users.userterritory1, creds: this.creds.userterritory1 },
          { name: 'userterritory2', data: this.users.userterritory2, creds: this.creds.userterritory2 },
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 },
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.admin)
    cy.getCreatedItemByName('territory1').then(territory => {
      cy.visit('/#/admin/territory?territory=' + territory.id)
    })

    cy.getCreatedItemByName('adminterritory1').then(user => {
      cy.get('[data-cy=assign-user-id-' + user.id + ']').click({ force: true })
    })
    cy.wait('@assignUser').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    cy.getCreatedItemByName('userterritory1').then(user => {
      cy.get('[data-cy=assign-user-id-' + user.id + ']').click({ force: true })
    })
    cy.wait('@assignUser').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    cy.getCreatedItemByName('adminterritory1').then(user => {
      cy.get('[data-cy=unassign-user-id-' + user.id + ']').click({ force: true })
    })
    cy.wait('@unassignUser').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.contains("désassigné")
    cy.get('[data-cy="users-refresh"]').click({ force: true })
    cy.wait('@getUser')
    cy.getCreatedItemByName('adminterritory1').then(user => {
      cy.get('[data-cy=unassign-user-id-' + user.id + ']').should('not.exist')
    })

    cy.get('[data-cy="manage-cities"]').click({ force: true })
    cy.contains("Aucune commune dans le territoire")
    cy.contains("Aucun résultat de recherche")

    cy.get('[data-cy="territory-search"] :input').clear({ force: true }).type("Loire", { force: true })
    cy.wait('@placeSearch').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.get('[data-cy="search-results"]').contains("Régions")
    cy.get('[data-cy="search-results"]').contains("Départements")
    cy.get('[data-cy="search-results"]').contains("Communes")

    cy.get('[data-cy="territory-search"] :input').clear({ force: true }).type("l'aber", { force: true })
    cy.wait('@placeSearch').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.get('[data-cy="add-city-1"]').click({ force: true })
    cy.wait('@assignCity').then(function (xhr) {
      expect(xhr.status).equals(201)
    })
    cy.get('[data-cy="add-city-2"]').click({ force: true })
    cy.wait('@assignCity').then(function (xhr) {
      expect(xhr.status).equals(201)
    })
    cy.get('[data-cy="unassign-city-1"]').click({ force: true })
    cy.wait('@unassignCity').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.contains("supprimée")
    cy.get('[data-cy="cities-refresh"]').click({ force: true })
    cy.get('[data-cy="unassign-city-1"]').should('not.exist')

    cy.get('[data-cy="delete-territory"]').click({ force: true })
    cy.get('.modal-buttons > :nth-child(2)').click({ force: true })
    cy.wait('@deleteTerritory').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
  })
})
