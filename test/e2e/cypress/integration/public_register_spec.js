
describe('Create account', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.logout()
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('User can register', function () {
    cy.visit('/')

    cy.contains('Connectez vous pour accéder à l\'application').should('be.visible')
    cy.contains('Connexion').should('be.visible')
    cy.contains('Mot de passe oublié?').should('be.visible')

    cy.contains('Créer mon compte').click()
    cy.get('[data-cy="register-firstname"] :input').type(this.users.particulier1.firstname)
    cy.get('[data-cy="register-lastname"] :input').type(this.users.particulier1.lastname)
    cy.get('[data-cy="register-email"] :input').type(this.creds.particulier1.username)
    cy.get('[data-cy="register-phone"] :input').type(this.users.particulier1.phone)
    cy.get('[data-cy="register-password"] :input').type(this.creds.particulier1.password)
    cy.get('[data-cy="register-passwordConfirm"] :input').type(this.creds.particulier1.password, { force: true })

    cy.get('[data-cy="register-submit"]').click()
    cy.wait('@createAccount').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    cy.getMailtrapInbox().then(response => {
      let messageId = response.body[0].id
      expect(response.body[0].to_email).equals(this.users.particulier1.email)
      cy.getMailtrapMessage(response.body[0].html_path).then(response => {
        cy.activateAccountFromMail(response.body).then(response => {
          cy.deleteMailtrapMessage(messageId)
          expect(response.status).equals(200)
        })
      })
    })

    cy.visit('/')
    cy.get('[data-cy="login-username"] :input').type(this.creds.particulier1.username)
    cy.get('[data-cy="login-password"] :input').type(this.creds.particulier1.password)
    cy.get('[data-cy="login-submit"]').click()
    cy.wait('@auth').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

  })
})
