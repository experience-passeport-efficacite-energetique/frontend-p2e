describe('Auidtor - passport', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport creation as auditor', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Assigning leads")
    cy.login(this.creds.auditor1)
    cy.getCreatedItemByName('lead1').then(lead => {
      cy.assignLead(lead)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.auditor1)
    cy.visit('/#/admin/leads')
    cy.wait('@getLead').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.getCreatedItemByName('lead1').then(lead => {
      cy.get('[data-cy="leads-lead-create-passport-id-' + lead.id + '"]').click()
    })


    cy.wait('@getVentilations')
    cy.wait('@getHeatings')
    cy.wait('@getDHW')
    cy.contains('Données générales')
    cy.get('[data-cy="passport-project"] .q-input-area').clear({ force: true }).type("Cypress Test Project", { force: true })
    cy.get('[data-cy="passport-constructionYear"] :input').clear({ force: true }).type("1969", { force: true })
    cy.get('[data-cy="passport-altitude"] :input').clear({ force: true }).type("152", { force: true })
    cy.get('[data-cy="passport-heatedSurface"] :input').clear({ force: true }).type("230", { force: true })
    cy.get('[data-cy="passport-generalComments"] .q-input-area').clear({ force: true }).type("No comment on Cypress project", { force: true })

    cy.get('[data-cy=passport-type]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-climateZone]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-surfaceType]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-adjencency]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-floors]').click()
    cy.get(':nth-child(2) > .q-item-main').click()

    cy.get('[data-cy=passport-btn-submit]').click()
    cy.wait('@createPassport')

    //walls data
    cy.get('[data-cy=passport-wallsData-add]').click()
    cy.get('[data-cy=passport-wallsData-state]').contains('État d\'usage').click()
    cy.get('[data-cy=passport-wallsData-rehabilitation]').contains('Isolation remplacée').click()
    cy.get('[data-cy="passport-wallsData-rehabilitationYear"] :input').type("1982", { force: true })
    cy.get('[data-cy=passport-wallsData-currentInsulation]').contains('ITE').click()
    cy.get('[data-cy=passport-wallsData-plannedInsulation]').contains('ITI').click()
    cy.get('[data-cy="passport-wallsData-comments"] .q-input-area').type("No comment on walls data", { force: true })

    //floor data
    cy.get('[data-cy=passport-floorData-add]').click()
    cy.get('[data-cy=passport-floorData-state]').contains('État proche du neuf').click()
    cy.get('[data-cy=passport-floorData-type]').contains('Terre-plein').click()
    cy.get('[data-cy=passport-floorData-rehabilitation]').contains('Isolation remplacée').click()
    cy.get('[data-cy="passport-floorData-rehabilitationYear"] :input').type("1999", { force: true })
    cy.get('[data-cy="passport-floorData-comment"] .q-input-area').type("No comment on floor data", { force: true })

    //roof data
    cy.get('[data-cy=passport-roofData-state]').contains('Nécessite des travaux').click()
    cy.get('[data-cy=passport-roofData-attictype]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-roofData-rehabilitation]').contains('Isolation d\'origine').click()
    cy.get('[data-cy="passport-roofData-comment"] .q-input-area').type("No comment on roof data", { force: true })

    //vents data
    cy.get('[data-cy=passport-ventsData-add]').click()
    cy.get('[data-cy=passport-ventsData-state]').contains('État d\'usage').click()
    cy.get('[data-cy=passport-ventsData-glazingType]').click()
    cy.get(':nth-child(3) > .q-item-main').click()
    cy.get('[data-cy=passport-ventsData-poseType]').contains('Tunnel').click()
    cy.get('[data-cy=passport-ventsData-rehabilitation]').contains('Ouvrants remplacés').click()
    cy.get('[data-cy="passport-ventsData-rehabilitationYear"] :input').type("1988", { force: true })
    cy.get('[data-cy="passport-ventsData-comments"] .q-input-area').type("No comment on vents data", { force: true })

    //ventilation data
    cy.get('[data-cy=passport-ventilationData-add]').click()
    cy.get('[data-cy=passport-ventilationData-state]').contains('Nécessite des travaux').click()
    cy.get('[data-cy=passport-ventilationData-currentVentilation]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-ventilationData-plannedVentilation]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-ventilationData-rehabilitation]').contains('Ventilation remplacée').click()
    cy.get('[data-cy="passport-ventilationData-rehabilitationYear"] :input').type("2010", { force: true })
    cy.get('[data-cy=passport-ventilationData-maintenance]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-ventilationData-comments"] .q-input-area').type("No comment on ventilation data", { force: true })

    //airtightness data
    cy.get('[data-cy=passport-airtightnessData-add]').click()
    cy.get('[data-cy="passport-airtightnessData-currentAirtightness"]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-airtightnessData-plannedAirtightness"]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-airtightnessData-comment"] .q-input-area').type("No comment on airtightness data", { force: true })

    //heating data
    cy.get('[data-cy=passport-heatingData-add]').click()
    cy.get('[data-cy=passport-heatingData-state]').contains('Nécessite des travaux').click()
    cy.get('[data-cy=passport-heatingData-currentHeating]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-heatingData-plannedHeating]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-heatingData-rehabilitation]').contains('Chauffage remplacé').click()
    cy.get('[data-cy="passport-heatingData-rehabilitationYear"] :input').type("2003", { force: true })
    cy.get('[data-cy=passport-heatingData-maintenance]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-heatingData-comment"] .q-input-area').type("No comment on heating data", { force: true })

    //thermostat data
    cy.get('[data-cy=passport-thermostatData-add]').click()
    cy.get('[data-cy=passport-thermostatData-presence]').contains('Oui').click()
    cy.get('[data-cy=passport-thermostatData-state]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-thermostatData-comment"] .q-input-area').type("No comment on thermostat data", { force: true })

    //dhw data
    cy.get('[data-cy=passport-dhwData-add]').click()
    cy.get('[data-cy=passport-dhwData-state]').contains('Nécessite des travaux').click()
    cy.get('[data-cy=passport-dhwData-currentDhw]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-dhwData-plannedDhw]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-dhwData-plannedSolar]').contains('Oui').click()
    cy.get('[data-cy=passport-dhwData-rehabilitation]').contains('Chauffage remplacé').click()
    cy.get('[data-cy=passport-dhwData-maintenance]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy="passport-dhwData-comment"] .q-input-area').type("No comment on dhw data", { force: true })

    //tdf data
    cy.get('[data-cy=passport-tdfData-add]').click()
    cy.get('[data-cy=passport-tdfData-electricalWiringState]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-electricalWiringLastControl]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-gasSystemState]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-plumbingState]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-plumbingStateLastControl]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-leadPresence]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-leadLastControl]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-asbestosPresence]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-asbestosLastControl]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-termitesPresence]').click()
    cy.get(':nth-child(2) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-termitesLastControl]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-epcsControlState]').click()
    cy.get(':nth-child(1) > .q-item-main').click()
    cy.get('[data-cy=passport-tdfData-septicTankIndividual]').contains('Oui').click()

    //submit
    cy.get('[data-cy=passport-btn-submit]').click()
    cy.wait('@updatePassport').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    // create synthesis
    cy.get('[data-cy=passport-btn-createSynthesis]').click()
    cy.wait('@getCombinatories').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
    cy.get('[data-cy="renovationPlan-improvement-combinatory"]').click()
    cy.get(':nth-child(2) > .q-item-main > .q-item-label').click()

    cy.get('[data-cy=renovationPlan-btn-submit]').click()
    cy.wait('@createRP').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

  })
})
