describe('Auditor - Login', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('test auditor login', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.visit('/#/login')
    cy.get('[data-cy="login-username"] :input').type(this.creds.auditor1.username)
    cy.get('[data-cy="login-password"] :input').type(this.creds.auditor1.password)
    cy.get('[data-cy="login-submit"]').click()
    cy.wait('@auth').then(function (xhr) {
      expect(xhr.status).equals(200)
    })
  })
})
