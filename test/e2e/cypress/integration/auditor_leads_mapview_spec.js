describe('Auditor - leads - Map view', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test leads list display + actions as auditor', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 },
          { name: 'lead2', data: this.leads.lead2 },
          { name: 'lead3', data: this.leads.lead3 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("# Assigning leads")
    cy.login(this.creds.auditor1)
    cy.getCreatedItemByName('lead1').then(lead => {
      cy.assignLead(lead)
    })

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.auditor1)
    cy.visit('/#/admin/leads')

    cy.get('[data-cy="leads-map-view"]').click()
    cy.contains('Distance').click()

    cy.getCreatedItemByName('lead1').then(lead => {
      cy.get('[data-cy="leaflet-map"]').should('exist')
    })
  })
})
