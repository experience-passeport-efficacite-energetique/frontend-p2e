describe('Admin - territories', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('territories').as('territories')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test passport list for territory admins', function () {
    var itemsToCreate = [
      {
        type: 'territories',
        items: [
          { name: 'territory1', data: this.territories.territory1 },
          { name: 'territory2', data: this.territories.territory2 },
          { name: 'territory3', data: this.territories.territory3 },
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")

    cy.login(this.creds.admin)
    cy.visit('/#/admin/territories')
    cy.wait('@getTerritory')

    cy.contains(this.territories.territory1.label)
    cy.contains(this.territories.territory2.label)
    cy.contains(this.territories.territory3.label)
    cy.get('tr').should('have.length', 4)

    cy.getCreatedItemByName('territory2').then(territory => {
      cy.get('[data-cy=territories-edit-id-' + territory.id + ']').click({ force: true })
    })

    cy.wait('@getTerritory').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

  })
})
