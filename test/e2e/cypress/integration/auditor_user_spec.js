describe('Auditor - User management', function () {

  before(function () {
    cy.serverStart()
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Create and activate user as auditor', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 }
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.auditor1)
    cy.visit('/#/admin/user')
    cy.get('[data-cy="user-firstname"] :input').type(this.users.particulier1.firstname)
    cy.get('[data-cy="user-lastname"] :input').type(this.users.particulier1.lastname)
    cy.get('[data-cy="user-email"] :input').type(this.users.particulier1.email)
    cy.get('[data-cy="user-phone"] :input').type(this.users.particulier1.phone)
    cy.get('[data-cy="user-address"] :input').type(this.users.particulier1.address.address)
    cy.wait('@searchAddress')
    cy.get(':nth-child(1) > .q-item-main').click()

    cy.get('[data-cy="user-submit"]').click()
    cy.wait('@createUser').then(function (xhr) {
      expect(xhr.status).equals(201)
    })

    cy.getMailtrapInbox().then(response => {
      let messageId = response.body[0].id
      expect(response.body[0].to_email).equals(this.users.particulier1.email)
      cy.getMailtrapMessage(response.body[0].html_path).then(response => {
        cy.activateAccountFromMail(response.body).then(response => {
          cy.deleteMailtrapMessage(messageId)
          expect(response.status).equals(200)
        })
      })
    })

  })

})
