describe('Admin - renovation plans', function () {


  before(function () {
    cy.serverStart()
    cy.fixture('leads').as('leads')
    cy.fixture('creds').as('creds')
    cy.fixture('users').as('users')
    cy.fixture('passports').as('passports')
    cy.fixture('renovationPlans').as('renovationPlans')
    cy.fixture('vigilancePoints').as('vigilancePoints')
    cy.fixture('vigilancePointsConditions').as('vigilancePointsConditions')
  })

  beforeEach(function () {
    cy.log("## CLEANING DB BEFORE TESTS")
    cy.cleanAll()
  })

  it('Test renovation plan readable for admin', function () {
    var itemsToCreate = [
      {
        type: 'users',
        items: [
          { name: 'auditor1', data: this.users.auditor1, creds: this.creds.auditor1 },
          { name: 'particulier1', data: this.users.particulier1, creds: this.creds.particulier1 }
        ]
      },
      {
        type: 'leads',
        as: this.creds.particulier1,
        items: [
          { name: 'lead1', data: this.leads.lead1 }
        ]
      },
      {
        type: 'passports',
        as: this.creds.auditor1,
        items: [
          { name: 'passport1', data: this.passports.passport1, user: this.users.particulier1, leadName: 'lead1' },
        ]
      },
      {
        type: 'renovationPlans',
        as: this.creds.auditor1,
        items: [
          { name: 'plan1', data: this.renovationPlans.plan1, passportName: 'passport1' },
        ]
      },
      {
        type: 'vigilancePoints',
        items: [
          { name: 'vigilancePoint1', data: this.vigilancePoints.vigilancePoint1 },
          { name: 'vigilancePoint2', data: this.vigilancePoints.vigilancePoint2 }
        ]
      },
      {
        type: 'vigilancePointsCondition',
        items: [
          { name: 'vigilancePointCondition2', data: this.vigilancePointsConditions.vigilancePointCondition2, vpName: 'vigilancePoint1' },
          { name: 'vigilancePointCondition3', data: this.vigilancePointsConditions.vigilancePointCondition3, vpName: 'vigilancePoint2' },
          { name: 'vigilancePointCondition4', data: this.vigilancePointsConditions.vigilancePointCondition4, vpName: 'vigilancePoint2'}
        ]
      }
    ]

    cy.log("## INITIALIZING TEST: POPULATING DB")
    cy.populateDb(itemsToCreate)

    cy.log("## DATA INITIALIZED - BEGINNING TESTS")
    cy.login(this.creds.admin)
    cy.visit('/#/admin/plans')
    cy.wait('@getRenovationPlan').then(function (xhr) {
      expect(xhr.status).equals(200)
    })

    cy.getCreatedItemByName('plan1').then(plan => {
      cy.get('[data-cy="renovationsplans-rp-edit-id-' + plan.id + '"]').click()

      cy.wait('@getRenovationPlan').then(function (xhr) {
        expect(xhr.status).equals(200)
      })

      cy.get('[data-cy="renovationsplans-status-draft-id-' + plan.id + '"]').click()

      cy.wait('@updatePassport').then(function (xhr) {
        expect(xhr.status).equals(200)
      })

      cy.wait('@computeVigilancePointCondition').then(function (xhr) {
        expect(xhr.status).equals(200)
      })

      cy.contains('Améliorations du logement possibles').should('be.visible')
      cy.contains('Lot').should('be.visible')
      cy.contains('Murs').should('be.visible')
      cy.contains('Plancher').should('be.visible')
      cy.contains('Toiture').should('be.visible')
      cy.contains('Ouvrant').should('be.visible')
      cy.contains('Chauffage').should('be.visible')
      cy.contains('Ventilation').should('be.visible')
      cy.contains('Gestion dynamique du chauffage').should('be.visible')
      cy.contains('ECS').should('be.visible')
      cy.contains('Étanchéité à l\'air').should('be.visible')

      cy.getCreatedItemByTypeAndId('passports', plan.passport.id).then(passport => {
        cy.get('[data-cy="renovationPlan-improvement-walls-comment"] .q-input-area').should('have.value',
          passport.wallsData.comment
        ).clear({ force: true }).type("New comment for walls", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-walls-projectComment"] .q-input-area').should('have.value',
          passport.wallsData.projectComment
        ).clear({ force: true }).type("New  project comment for walls", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-floor-comment"] .q-input-area').should('have.value',
          passport.floorData.comment
        ).clear({ force: true }).type("New comment for floor", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-roof-comment"] .q-input-area').should('have.value',
          passport.roofData.comment
        ).clear({ force: true }).type("New comment for roof", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-vents-comment"] .q-input-area').should('have.value',
          passport.ventsData.comment
        ).clear({ force: true }).type("New comment for vents", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-heating-comment"] .q-input-area').should('have.value',
          passport.ventsData.comment
        ).clear({ force: true }).type("New comment for heating", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-ventilation-comment"] .q-input-area').should('have.value',
          passport.ventilationData.comment
        ).clear({ force: true }).type("New comment for ventilations", { force: true })
        cy.get('[data-cy="phase-true"] [data-cy="renovationPlan-improvement-airtightness-comment"] .q-input-area').should('have.value',
          passport.airtightnessData.comment
        ).clear({ force: true }).type("New comment for airtightness", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-thermostat-comment"] .q-input-area').should('have.value',
          passport.thermostatData.comment
        ).clear({ force: true }).type("New comment for thermostat", { force: true })
        cy.get('[data-cy="renovationPlan-improvement-dhw-comment"] .q-input-area').should('have.value',
          passport.dhwData.comment
        ).clear({ force: true }).type("New comment for dhw", { force: true })
        cy.get('[data-cy="phase-true"] [data-cy="renovationPlan-improvement-airtightness-comment"] .q-input-area').should('have.value',
          "New comment for airtightness"
        ).clear({ force: true })
      })

      cy.get('[data-cy="walls-defined-price-min"] .q-input-target').clear({ force: true }).type("9988", { force: true })
      plan.improvement.wallsDefinedPriceMin = 9988;
      cy.get('[data-cy="floor-defined-price-max"] .q-input-target').clear({ force: true }).type("8877", { force: true })
      plan.improvement.floorDefinedPriceMax = 8877;
      cy.get('[data-cy="dhw-defined-price-min"] .q-input-target').clear({ force: true }).type("7766", { force: true })
      plan.improvement.dhwDefinedPriceMin = 7766;
      cy.get('[data-cy="electricalWiring-defined-price-max"] .q-input-target').clear({ force: true }).type("6655", { force: true })
      plan.tdf.electricalWiringDefinedPriceMax = 6655;
      cy.get('[data-cy="plumbing-defined-price-min"] .q-input-target').clear({ force: true }).type("5544", { force: true })
      plan.tdf.plumbingDefinedPriceMin = 5544;
      cy.get('[data-cy="roof-defined-price-max"] .q-input-target').clear({ force: true }).type("4433", { force: true })
      plan.improvement.roofDefinedPriceMax = 4433;
      cy.get('[data-cy="vents-defined-price-max"] .q-input-target').clear({ force: true }).type("3322", { force: true })
      plan.improvement.ventsDefinedPriceMax = 3322;
      cy.get('[data-cy="vents-defined-price-min"] .q-input-target').clear({ force: true }).type("99", { force: true })
      plan.improvement.ventsDefinedPriceMin = 99;

      cy.contains('Installation électrique').should('be.visible')
      cy.contains('Plomberie').should('be.visible')

      cy.contains('Coût des travaux').should('be.visible')
      cy.contains('TOTAL').should('be.visible')

      cy.get('[data-cy="asbestos-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-true"] [data-cy="plumbing-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="lead-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="termites-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="roof-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="floor-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="dhw-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="heating-switch-phase-btn"]').should('not.exist')

      cy.getRenovationPlanPriceRanges(plan.id).then(priceRanges => {

        let allPhaseItems = [
          'walls', 'floor', 'roof', 'vents', 'dhw', 'heating', 'ventilation', 'airtightness', 'thermostat',
          'electricalWiring', 'plumbing'
        ]

        cy.getPhasePrices(plan, priceRanges, true, allPhaseItems).then(phaseTruePrices => {
          cy.getPhasePrices(plan, priceRanges, false, allPhaseItems).then(phaseFalsePrices => {
            cy.get('[data-cy="phase-total-renovationPlan-prices-default-max"] .q-input-target').should('have.value',
              (phaseTruePrices.max + phaseFalsePrices.max).toString()
            )
            cy.get('[data-cy="phase-total-renovationPlan-prices-default-min"] .q-input-target').should('have.value',
              (phaseTruePrices.min + phaseFalsePrices.min).toString()
            )

            cy.get('[data-cy="phase-true-renovationPlan-prices-default-max"] .q-input-target').should('have.value',
              phaseTruePrices.max.toString()
            )
            cy.get('[data-cy="phase-true-renovationPlan-prices-default-min"] .q-input-target').should('have.value',
              phaseTruePrices.min.toString()
            )

            cy.get('[data-cy="phase-false-renovationPlan-prices-default-max"] .q-input-target').should('have.value',
              phaseFalsePrices.max.toString()
            )
            cy.get('[data-cy="phase-false-renovationPlan-prices-default-min"] .q-input-target').should('have.value',
              phaseFalsePrices.min.toString()
            )


            cy.get('[data-cy="phase-true-renovationPlan-prices-defined-max"] .q-input-target').should('have.value',
              phaseTruePrices.definedMax.toString()
            )
            cy.get('[data-cy="phase-true-renovationPlan-prices-defined-min"] .q-input-target').should('have.value',
              phaseTruePrices.definedMin.toString()
            )

            cy.get('[data-cy="phase-false-renovationPlan-prices-defined-max"] .q-input-target').should('have.value',
              phaseFalsePrices.definedMax.toString()
            )
            cy.get('[data-cy="phase-false-renovationPlan-prices-defined-min"] .q-input-target').should('have.value',
              phaseFalsePrices.definedMin.toString()
            )
            cy.get('[data-cy="phase-total-renovationPlan-prices-defined-max"] .q-input-target').should('have.value',
              (phaseTruePrices.definedMax + phaseFalsePrices.definedMax).toString()
            )
            cy.get('[data-cy="phase-total-renovationPlan-prices-defined-min"] .q-input-target').should('have.value',
              (phaseTruePrices.definedMin + phaseFalsePrices.definedMin).toString()
            )
          })
        })
      })

      //check vigilant point
      cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('be.visible')
      })
      cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('be.visible')
      })
      cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-disable-id-' + vigilancePoint.id + ']').click({ force: true })
      })
      cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-handle-content-' + vigilancePoint.id + ']').click({ force: true })
      })
      cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('be.visible')
      })
      cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('not.exist')
      })
      cy.get('[data-cy="vigilance-point-info-empty"]').should('not.exist')

      //check handle & disable on 2VP
      cy.get('[data-cy="phase-true"] [data-cy="ventilation-switch-phase-btn"]').click()
      cy.wait('@updatePassport').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@updateRP').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@computeVigilancePointCondition').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('be.visible')
      })
      cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-description-' + vigilancePoint.id + ']').should('not.exist')
      })

      //test 1 VP
      cy.get('[data-cy="phase-true"] [data-cy="roof-switch-phase-btn"]').click()
      cy.wait('@updatePassport').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@updateRP').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@computeVigilancePointCondition').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.getCreatedItemByName('vigilancePoint1').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-disable-id-' + vigilancePoint.id + ']').should('be.visible')
      })
      cy.getCreatedItemByName('vigilancePoint2').then(vigilancePoint => {
        cy.get('[data-cy=vigilance-point-disable-id-' + vigilancePoint.id + ']').should('not.exist')
      })

      //test 0 VP
      cy.get('[data-cy="phase-true"] [data-cy="heating-switch-phase-btn"]').click()
      cy.wait('@updatePassport').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@updateRP').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.wait('@computeVigilancePointCondition').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
      cy.get('[data-cy="vigilance-point-info-empty"]').should('be.visible')

      cy.get('[data-cy="phase-true"] [data-cy="dhw-switch-phase-btn"]').click()
      cy.get('[data-cy="phase-false"] [data-cy="plumbing-switch-phase-btn"]').click()

      cy.get('[data-cy="phase-true"] [data-cy="dhw-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-true"] [data-cy="heating-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="termites-switch-phase-btn"]').should('not.exist')
      cy.get('[data-cy="phase-false"] [data-cy="plumbing-switch-phase-btn"]').should('not.exist')


      cy.get('[data-cy=renovationPlan-btn-submit]').click()
      cy.wait('@updateRP').then(function (xhr) {
        expect(xhr.status).equals(200)
      })
    })
  })
})
