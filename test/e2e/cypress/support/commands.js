// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

var createdItems = []

const DEFAULT_NB_RETRY = 10
const RETRY_DELAY = 2000

//contains the singular version of plurals words
var singular = {
  leads: 'lead',
  users: 'user',
  passports: 'passport',
  territories: 'territory',
}

Cypress.Commands.add('getCreatedItems', () => {
  return createdItems
})

Cypress.Commands.add('getCreatedItemByName', (names) => {
  if (!names) return undefined
  if (Array.isArray(names)) {
    let targetItems = {}
    createdItems.forEach(item => {
      if (names.includes(item.name)) {
        targetItems[item.name] = item.data
      }
    })
    return targetItems
  } else {
    return createdItems.find((item) => {
      return item.name === names
    }).data
  }
})

Cypress.Commands.add('getCreatedItemByTypeAndId', (type, id) => {
  return createdItems.find((item) => {
    return item.type === type && item.data.id == id
  }).data
})

Cypress.Commands.add('serverStart', () => {
  cy.server()
  cy.route('GET', '/search/*').as('searchAddress')
  cy.route('GET', Cypress.env('apiUrl') + '/territories/*').as('getTerritory')
  cy.route('GET', Cypress.env('apiUrl') + '/users/*').as('getUser')
  cy.route('GET', Cypress.env('apiUrl') + '/leads/*').as('getLead')
  cy.route('GET', Cypress.env('apiUrl') + '/passports/*').as('getPassport')
  cy.route('GET', Cypress.env('apiUrl') + '/renovationPlans/*').as('getRenovationPlan')
  cy.route('GET', Cypress.env('apiUrl') + '/ventilations/*').as('getVentilations')
  cy.route('GET', Cypress.env('apiUrl') + '/heatings/*').as('getHeatings')
  cy.route('GET', Cypress.env('apiUrl') + '/domesticHotWaters/*').as('getDHW')
  cy.route('GET', Cypress.env('apiUrl') + '/combinatories/*').as('getCombinatories')
  cy.route('GET', Cypress.env('apiUrl') + '/places?search*').as('placeSearch')
  cy.route('GET', Cypress.env('apiUrl') + '/vigilancePoints/*').as('getVigilancePoint')
  cy.route('POST', Cypress.env('apiUrl') + '/auth').as('auth')
  cy.route('POST', Cypress.env('apiUrl') + '/account/create').as('createAccount')
  cy.route('POST', Cypress.env('apiUrl') + '/users/').as('createUser')
  cy.route('POST', Cypress.env('apiUrl') + '/leads/').as('createLead')
  cy.route('POST', Cypress.env('apiUrl') + '/vigilancePoints/').as('createVigilancePoint')
  cy.route('POST', Cypress.env('apiUrl') + '/vigilancePoints/*/addCondition').as('addVigilancePointCondition')
  cy.route('POST', Cypress.env('apiUrl') + '/renovationPlans/*/computeVps').as('computeVigilancePointCondition')
  cy.route('POST', Cypress.env('apiUrl') + '/renovationPlans/').as('createRP')
  cy.route('POST', Cypress.env('apiUrl') + '/passports/').as('createPassport')
  cy.route('POST', Cypress.env('apiUrl') + '/my/assignments').as('assignLead')
  cy.route('POST', Cypress.env('apiUrl') + '/territories/*/user-assignment').as('assignUser')
  cy.route('POST', Cypress.env('apiUrl') + '/territories/*/city-assignment').as('assignCity')
  cy.route('PATCH', Cypress.env('apiUrl') + '/users/*').as('updateUser')
  cy.route('PATCH', Cypress.env('apiUrl') + '/passports/*').as('updatePassport')
  cy.route('PATCH', Cypress.env('apiUrl') + '/renovationPlans/*').as('updateRP')
  cy.route('PATCH', Cypress.env('apiUrl') + '/vigilancePoints/*').as('updateVigilancePoint')
  cy.route('PATCH', Cypress.env('apiUrl') + '/vigilancePoints/*/updateCondition/*').as('updateVigilancePointCondition')
  cy.route('DELETE', Cypress.env('apiUrl') + '/territories/*/user-assignment').as('unassignUser')
  cy.route('DELETE', Cypress.env('apiUrl') + '/territories/*/city-assignment').as('unassignCity')
  cy.route('DELETE', Cypress.env('apiUrl') + '/territories/*').as('deleteTerritory')
  cy.route('DELETE', Cypress.env('apiUrl') + '/vigilancePoints/*').as('deleteVigilancePoint')
  cy.route('DELETE', Cypress.env('apiUrl') + '/vigilancePoints/*/removeCondition/*').as('deleteVigilancePointCondition')
})

Cypress.Commands.add("logout", () => {
  cy.log("# Logging out")
  window.localStorage.removeItem('jwt')
  window.localStorage.removeItem('jwt.time')
  cy.clearLocalStorage()
})

Cypress.Commands.add("login", (creds) => {
  cy.logout()
  cy.log("# Login with username: " + creds.username)
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/auth',
    body: {
      username: creds.username,
      password: creds.password
    }
  }).then(response => {
    window.localStorage.setItem('jwt', response.body.token)
    window.localStorage.setItem('jwt.time', Date.now())
    return response
  })
})

Cypress.Commands.add("create", (itemType, itemData) => {
  cy.log("# Creating " + itemType)
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/' + itemType + '/',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: itemData
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("fetchAll", (itemType) => {
  cy.log("# Fetching all " + itemType)
  return cy.request({
    method: 'GET',
    url: Cypress.env('apiUrl') + '/' + itemType + '/?perpage=10000',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt'),
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("deleteAll", (itemType) => {
  cy.log("# Deleting all " + itemType)
  cy.fetchAll(itemType).then(response => {
    response.body[itemType].forEach((item) => {
      cy.delete(itemType, item.id)
    })
  })
})

Cypress.Commands.add("createLead", (leadData, ownerName) => {
  cy.log("# Creating lead with owner: " + ownerName)
  return cy.getCreatedItemByName(ownerName).then(owner => {
    let finalLeadData = JSON.parse(JSON.stringify(leadData))
    if (owner) finalLeadData.owner = {
      id: owner.id
    }
    return cy.request({
      method: 'POST',
      url: Cypress.env('apiUrl') + '/leads/',
      headers: {
        Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
      },
      body: finalLeadData
    })
  }).then(response => {
    return response.body.lead
  })
})

Cypress.Commands.add("createPassport", (passportData, leadName, user, as) => {
  cy.log("# Creating passport form lead with name: " + leadName + " using address from user with email: " + user.email)
  return cy.getCreatedItemByName(leadName).then(lead => {
    if (as && !lead.assigned) {
      cy.assignLead(lead)
    }
    let finalPassportData = JSON.parse(JSON.stringify(passportData))
    finalPassportData.lead.id = lead.id
    finalPassportData.address = user.address
    return cy.request({
      method: 'POST',
      url: Cypress.env('apiUrl') + '/passports/',
      headers: {
        Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
      },
      body: finalPassportData
    })
  }).then(response => {
    return response.body.passport
  })
})

Cypress.Commands.add("createRenovationPlan", (planData, passportName, user) => {
  cy.log("# Creating renovation plan form passport with name: " + passportName)
  return cy.getCreatedItemByName(passportName).then(passport => {
    let finalPlanData = JSON.parse(JSON.stringify(planData))
    finalPlanData.passport.id = passport.id
    return cy.request({
      method: 'POST',
      url: Cypress.env('apiUrl') + '/renovationPlans/',
      headers: {
        Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
      },
      body: finalPlanData
    })
  }).then(response => {
    return response.body.renovationPlan
  })
})


Cypress.Commands.add("getRenovationPlanPriceRanges", (planId) => {
  return cy.request({
    method: 'GET',
    url: Cypress.env('apiUrl') + '/renovationPlans/' + planId + '/priceRanges',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    }
  }).then(response => {
    return response.body
  })
})

Cypress.Commands.add("delete", (itemType, itemId) => {
  if (itemType === 'users' && itemId === 1) {
    //not allowed to delete admin user
    return null
  }
  cy.log("# Deleting " + itemType + " with id " + itemId)
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + '/' + itemType + '/' + itemId,
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("cleanMailtrapInbox", () => {
  return cy.request({
    method: 'PATCH',
    url: Cypress.env('mailtrapUrl') + '/api/v1/inboxes/' + Cypress.env('mailtrapInboxId') + '/clean',
    headers: {
      'Api-Token': Cypress.env('mailtrapApiToken')
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("getMailtrapInbox", () => {
  return cy.request({
    method: 'GET',
    url: Cypress.env('mailtrapUrl') + '/api/v1/inboxes/' + Cypress.env('mailtrapInboxId') + '/messages',
    headers: {
      'Api-Token': Cypress.env('mailtrapApiToken')
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("findMailtrapMessage", (params, retry = DEFAULT_NB_RETRY, nbExpectedResults = -1) => {
  cy.log("Trying to find messages in Mailtrap inbox with params:" + JSON.stringify(params))
  return cy.getMailtrapInbox().then(response => {
    let messages = response.body
    let foundMessages = [];
    for (let messageIndex in messages) {
      let messageMatch = true;

      if (params.to) {
        messageMatch &= messages[messageIndex].to_email === params.to;
      }
      if (params.from) {
        messageMatch &= messages[messageIndex].from_email === params.from;
      }
      if (params.subject) {
        if (params.strictSubject) {
          messageMatch &= messages[messageIndex].subject === params.subject;
        } else {
          messageMatch &= messages[messageIndex].subject.includes(params.subject);
        }
      }
      if (messageMatch) {
        foundMessages.push(messages[messageIndex]);
        if (params.onlyFirst) {
          break;
        }
      }
    }
    if (nbExpectedResults !== -1 && foundMessages.length !== nbExpectedResults) {
      if (retry > 0) {
        cy.wait(RETRY_DELAY)
        console.log("WARNING - failed expected number of emails (found " + foundMessages.length + " intead of " + nbExpectedResults + " expected) - retrying after " + RETRY_DELAY + "ms (" + retry + " attempt(s) remaining)");
        return cy.findMailtrapMessage(params, retry - 1, nbExpectedResults);
      } else {
        console.warn("WARNING - failed to retrieve all expected emails");
        return Promise.resolve(foundMessages)
      }
    } else {
      return Promise.resolve(foundMessages)
    }
  })
})

Cypress.Commands.add("getMailtrapMessage", (messageHtmlPath) => {
  return cy.request({
    method: 'GET',
    url: Cypress.env('mailtrapUrl') + messageHtmlPath,
    headers: {
      'Api-Token': Cypress.env('mailtrapApiToken')
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("deleteMailtrapMessage", (id) => {
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('mailtrapUrl') + '/api/v1/inboxes/' + Cypress.env('mailtrapInboxId') + '/messages/' + id,
    headers: {
      'Api-Token': Cypress.env('mailtrapApiToken')
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("activateAccountFromMail", (mailBody, password) => {
  let tokenUrl = Cypress.env('accountActivationUrl').replace("{token}", "([a-f0-9]+)")
  tokenUrl = tokenUrl.replace(new RegExp(/\?/, 'g'), '\\?')
  let regex = new RegExp(tokenUrl, "g")
  let token = regex.exec(mailBody)[1]

  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiAccountActivationPath').replace('{token}', token),
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: {
      password: password ? password : "passe"
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("assignLead", (lead) => {
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/my/assignments',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: {
      lead: {
        id: lead.id
      }
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("assignUserToTerritory", (territory, user) => {
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/territories/' + territory.id + '/user-assignment',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: {
      user: {
        id: user.id
      }
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("assignCityToTerritory", (territory, cityId) => {
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/territories/' + territory.id + '/city-assignment',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: {
      city: {
        id: cityId
      }
    }
  }).then(response => {
    return response
  })
})

Cypress.Commands.add("createUser", (userInfo, userCreds) => {
  var createdUser
  cy.log("# Creating user with email: " + userInfo.email)
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/users/',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: Object.assign({}, userInfo, {
      activationUrl: Cypress.env('accountActivationUrl')
    })
  }).then(response => {
    createdUser = response.body.user
    return cy.fixture('creds')
  }).then(creds => {
    return cy.findMailtrapMessage({
      to: userCreds.username,
      from: creds.admin.username,
      subject: "Passeport Efficacité Energétique – Création de votre compte"
    }, DEFAULT_NB_RETRY, 1)
  }).then(response => {
    return cy.getMailtrapMessage(response[0].html_path)
  }).then(response => {
    return cy.activateAccountFromMail(response.body, userCreds.password)
  }).then(response => {
    expect(response.status).equals(200)
    return Promise.resolve(createdUser)
  })
})

Cypress.Commands.add("createVigilancePoint", (vigilancePointData) => {
  cy.log("# Creating vigilance point")
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/vigilancePoints/',
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    },
    body: JSON.parse(JSON.stringify(vigilancePointData))
  })
  .then(response => {
    return response.body.vigilancePoint
  })
})

Cypress.Commands.add("addVigilancePointCondition", (conditionData, vigilancePointName) => {
  cy.log("# Creating condition for vigilance point with name: " + vigilancePointName)
  return cy.getCreatedItemByName(vigilancePointName).then(vigilancePoint => {
    return cy.request({
      method: 'POST',
      url: `${Cypress.env('apiUrl')}/vigilancePoints/${vigilancePoint.id}/addCondition`,
      headers: {
        Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
      },
      body: JSON.parse(JSON.stringify(conditionData))
    })
  }).then(response => {
    return response.body.vigilancePoint.conditions.reduce((acc, current) => {
      acc = acc ? (acc.id > current.id ? acc : current) : current
    })
  })
})

Cypress.Commands.add("computeVigilancePoint", (renovationPlanId) => {
  cy.log("# Compute vigilance point for renovation plan")
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + '/renovationPlans/' + renovationPlanId + "/computeVps",
    headers: {
      Authorization: 'Bearer ' + window.localStorage.getItem('jwt')
    }
  })
    .then(response => {
      return response.body
    })
})


Cypress.Commands.add("populateDb", (itemsToCreate) => {
  itemsToCreate.forEach((itemList) => {
    if (!!itemList.as) {
      cy.login(itemList.as)
    } else {
      cy.fixture('creds').then(response => {
        cy.login(response.admin)
      })
    }
    switch (itemList.type) {
      case 'users':
        itemList.items.forEach((item) => {
          cy.createUser(item.data, item.creds).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      case 'renovationPlans':
        itemList.items.forEach((item) => {
          cy.createRenovationPlan(item.data, item.passportName).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      case 'passports':
        itemList.items.forEach((item) => {
          cy.createPassport(item.data, item.leadName, item.user, itemList.as).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      case 'leads':
        itemList.items.forEach((item) => {
          cy.createLead(item.data, item.owner).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      case 'vigilancePoints':
        itemList.items.forEach((item) => {
          cy.createVigilancePoint(item.data).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      case 'vigilancePointsCondition':
        itemList.items.forEach((item) => {
          cy.addVigilancePointCondition(item.data, item.vpName).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response
            })
          })
        })
        break
      default:
        itemList.items.forEach((item) => {
          cy.create(itemList.type, item.data).then(response => {
            createdItems.unshift({
              name: item.name,
              type: itemList.type,
              data: response.body[singular[itemList.type]]
            })
          })
        })
        break
    }
    cy.logout()
  })
  return undefined
})

Cypress.Commands.add("cleanDb", () => {
  cy.fixture('creds').then(response => {
    cy.login(response.admin)
  })
  createdItems.forEach((item) => {
    cy.delete(item.type, item.data.id)
  })
  cy.logout()
  return undefined
})

Cypress.Commands.add("resetDb", () => {
  createdItems = []
  cy.fixture('creds').then(response => {
    cy.login(response.admin)
  })
  cy.deleteAll('renovationPlans')
  cy.deleteAll('passports')
  cy.deleteAll('leads')
  cy.deleteAll('territories')
  cy.deleteAll('users')
  cy.deleteAll('vigilancePoints')
  cy.logout()
  return undefined
})

Cypress.Commands.add("cleanAll", () => {
  cy.resetDb()
  cy.cleanMailtrapInbox()
  return undefined
})

Cypress.Commands.add("getPhasePrices", (plan, priceRanges, phase, allItems) => {
  let phaseItems = {
    true: () => {
      return allItems.filter(item => {
        return plan.improvement[item + 'Selected'] || plan.tdf[item + 'Selected'] || item === 'airtightness'
      })
    },
    false: () => {
      return allItems.filter(item => {
        return (!plan.improvement[item + 'Selected'] && !plan.tdf[item + 'Selected']) || item === 'airtightness'
      })
    }
  }

  return {
    max: Math.round(Object.keys(priceRanges).reduce((previousCategoryTotal, categoryKey) => {
      return previousCategoryTotal + phaseItems[phase](allItems).reduce((previousTotal, itemKey) => {
        return previousTotal + (priceRanges[categoryKey][itemKey] ? priceRanges[categoryKey][itemKey].max : 0)
      }, 0)
    }, 0) / 1000) * 1000,
    min: Math.round(Object.keys(priceRanges).reduce((previousCategoryTotal, categoryKey) => {
      return previousCategoryTotal + phaseItems[phase](allItems).reduce((previousTotal, itemKey) => {
        return previousTotal + (priceRanges[categoryKey][itemKey] ? priceRanges[categoryKey][itemKey].min : 0)
      }, 0)
    }, 0) / 1000) * 1000,
    definedMax: Math.round(Object.keys(priceRanges).reduce((previousCategoryTotal, categoryKey) => {
      return previousCategoryTotal + phaseItems[phase](allItems).reduce((previousTotal, itemKey) => {
        return previousTotal + (plan[categoryKey][itemKey + 'DefinedPriceMax'] ?
          plan[categoryKey][itemKey + 'DefinedPriceMax'] :
          priceRanges[categoryKey][itemKey] ? priceRanges[categoryKey][itemKey].max : 0)
      }, 0)
    }, 0) / 1000) * 1000,
    definedMin: Math.round(Object.keys(priceRanges).reduce((previousCategoryTotal, categoryKey) => {
      return previousCategoryTotal + phaseItems[phase](allItems).reduce((previousTotal, itemKey) => {
        return previousTotal + (plan[categoryKey][itemKey + 'DefinedPriceMin'] ?
          plan[categoryKey][itemKey + 'DefinedPriceMin'] :
          priceRanges[categoryKey][itemKey] ? priceRanges[categoryKey][itemKey].min : 0)
      }, 0)
    }, 0) / 1000) * 1000
  }
})
