import { mount, createLocalVue } from '@vue/test-utils'
import Quasar from 'quasar'
import VueRouter from 'vue-router'
import VueLocalStorage from 'vue-local-storage'
import VueResource from 'vue-resource'
import Notifier from 'plugins/notifier'
import Api from 'plugins/api/Api'

export const LocalSettings = {
  apiDomain: ''
}

function getApi () {
  return new Api(LocalSettings.apiDomain)
}

export function mountWithDefaults (Component, options = {}) {
  const localVue = createLocalVue()
  localVue.use(Quasar)
  localVue.use(VueRouter)
  localVue.use(VueLocalStorage)
  localVue.use(VueResource)
  localVue.use(Notifier)
  localVue.use(getApi())
  const wrapper = mount(Component, { localVue, ...options })

  return wrapper
}

export function mountWithRoutes (Component, routes = [], connection = null, options = {}) {
  const localVue = createLocalVue()
  localVue.use(Quasar)
  localVue.use(VueRouter)
  localVue.use(VueLocalStorage)
  localVue.use(VueResource)
  localVue.use(Notifier)
  localVue.use(getApi())
  const router = new VueRouter({
    routes
  })
  const wrapper = mount(Component, { localVue, router, ...options })
  if (connection) {
    wrapper.vm.$api.connection = connection
    wrapper.update()
  }
  return wrapper
}

export const adminConnection = {
  user: {
    username: 'adminP2E',
    profiles: ['Admin']
  }
}

export const auditorConnection = {
  user: {
    username: 'adminP2E',
    profiles: ['Auditor']
  }
}
