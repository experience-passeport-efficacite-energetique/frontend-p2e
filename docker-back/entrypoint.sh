#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

php bin/console schema:apply --assume-yes;
php bin/console db:load etc/db_static_data.yml;

exec "$@"