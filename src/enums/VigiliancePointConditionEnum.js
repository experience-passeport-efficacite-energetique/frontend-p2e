import { AirtightnessEnum } from './improvement/AirtightnessEnum'
import { DhwOptions } from './improvement/DhwEnum'
import { FloorFormOption } from './improvement/FloorEnum'
import { HeatingsFormOptions } from './improvement/HeatingEnum'
import { RoofFormOptions } from './improvement/RoofEnum'
import { ThermostatOptions } from './improvement/ThermotstatEnum'
import { VentilationsFormOptions } from './improvement/VentilationEnum'
import { VentsFormOptions } from './improvement/VentsEnum'
import { WallFormOptions } from './improvement/WallEnum'

const AIRTIGHTNESS = 'airtightness'
const DHW_SOLAR = 'dwh.solar'
const FLOOR_TYPE = 'floor.floorType'
const HEATING = 'heating.id'
const ROOF_ATTIC = 'roof.atticType'
const THERMOSTAT = 'thermostat'
const VENTILATION = 'ventilation.id'
const VENTS_POSE = 'vents.poseType'
const WALLS_INSULATION = 'walls.insulationType'

export const lotsOptionsList = [
  { value: AIRTIGHTNESS, label: 'Étanchéité à l\'air' },
  { value: DHW_SOLAR, label: 'Eau chaude sanitaire' },
  { value: FLOOR_TYPE, label: 'Plancher Bas' },
  { value: HEATING, label: 'Chauffage' },
  { value: ROOF_ATTIC, label: 'Toitures & Combles' },
  { value: THERMOSTAT, label: 'Gestion dynamique du chauffage' },
  { value: VENTILATION, label: 'Ventilation' },
  { value: VENTS_POSE, label: 'Ouvrant' },
  { value: WALLS_INSULATION, label: 'Murs - Isolation' }
]

export const lotsValuesOptionsList = {
  [AIRTIGHTNESS]: AirtightnessEnum.values,
  [DHW_SOLAR]: DhwOptions.solar,
  [FLOOR_TYPE]: FloorFormOption.type,
  [HEATING]: HeatingsFormOptions.heatings,
  [ROOF_ATTIC]: RoofFormOptions.atticType,
  [THERMOSTAT]: ThermostatOptions.values,
  [VENTILATION]: VentilationsFormOptions.ventilations,
  [VENTS_POSE]: [{ value: null, label: 'Peu importe', 'preceding': true, 'following': true }].concat(VentsFormOptions.poseType),
  [WALLS_INSULATION]: WallFormOptions.insulationType
}
