export default {
  staticType: {
    THERMOSTAT: 'Autre',
    AIRTIGHTNESS: 'Autre',
    TDF_ASBESTOS: 'DDT',
    TDF_EPCS: 'DDT',
    TDF_LEAD: 'DDT',
    TDF_PLUMBING: 'DDT',
    TDF_TERMITES: 'DDT',
    TDF_ELECTRICALWIRING: 'DDT',
    TDF_GASSYSTEM: 'DDT',
    TDF_SEPTICTANK: 'DDT'
  },
  staticLabel: {
    THERMOSTAT: 'Régulation thermique',
    AIRTIGHTNESS: "Étanchéité à l'air",
    TDF_ASBESTOS: 'Traitement amiante',
    TDF_EPCS: 'EPCS',
    TDF_LEAD: 'Traitement plomb',
    TDF_PLUMBING: 'Plomberie',
    TDF_TERMITES: 'Traitement termites',
    TDF_ELECTRICALWIRING: 'Installation électrique',
    TDF_GASSYSTEM: 'Système de gaz',
    TDF_SEPTICTANK: 'Fausse septique'
  },
  priceUnit: {
    FIXED: '€ HT',
    UNIT: '€ HT/U',
    SURFACE: '€ HT/m²'
  }
}
