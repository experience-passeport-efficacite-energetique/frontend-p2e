import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const VentilationsFormOptions = {
  ventilations: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: '1', label: 'Naturelle', 'preceding': true, 'following': false },
    { value: '2', label: 'Auto-réglable', 'preceding': true, 'following': true },
    { value: '3', label: 'Hygro-réglable', 'preceding': true, 'following': true },
    { value: '4', label: 'Double flux', 'preceding': true, 'following': true }
  ]
}

export default {
  ventilations: convertFormOptionsToEnum(VentilationsFormOptions.ventilations)
}
