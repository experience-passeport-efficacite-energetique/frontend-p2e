import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const WallFormOptions = {
  insulationType: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: 'NONE', label: 'Sans isolation', 'preceding': true, 'following': false },
    { value: 'ITI', label: 'ITI' },
    { value: 'ETI', label: 'ITE' }
  ]
}

export default {
  insulationType: convertFormOptionsToEnum(WallFormOptions.insulationType)
}
