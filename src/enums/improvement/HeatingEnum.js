import { convertFormOptionsToEnum } from '../../utils/EnumUtils'

export const HeatingsFormOptions = {
  heatings: [
    { value: null, label: 'Peu importe', 'preceding': true, 'following': true },
    { value: '1', label: 'Chaudière gaz condensation', 'preceding': true, 'following': true },
    { value: '2', label: 'Chaudière fioul', 'preceding': true, 'following': true },
    { value: '3', label: 'PAC électrique COP > 3,5', 'preceding': true, 'following': true },
    { value: '4', label: 'Chaudière bois', 'preceding': true, 'following': true },
    { value: '5', label: 'Poêle / Insert bois', 'preceding': true, 'following': true },
    { value: '6', label: 'Ecogénérateur', 'preceding': true, 'following': true },
    { value: '7', label: 'Convecteur électrique', 'preceding': true, 'following': true },
    { value: '8', label: 'Pas de système de chauffage', 'preceding': true, 'following': false },
    { value: '9', label: 'Convecteurs électriques NFC', 'preceding': true, 'following': false },
    { value: '10', label: 'Radiateurs électriques NFC', 'preceding': true, 'following': false },
    { value: '11', label: 'Plafond rayonnant électrique', 'preceding': true, 'following': false },
    { value: '12', label: 'Plancher rayonnant électrique', 'preceding': true, 'following': false },
    { value: '13', label: 'Appareil électrique à accumulation', 'preceding': true, 'following': false },
    { value: '14', label: 'Plancher électrique à accumulation', 'preceding': true, 'following': false },
    { value: '15', label: 'Electrique direct autre', 'preceding': true, 'following': false },
    { value: '16', label: 'PAC (divisé) - type split', 'preceding': true, 'following': false },
    { value: '17', label: 'Radiateurs gaz à ventouse', 'preceding': true, 'following': false },
    { value: '18', label: 'Radiateurs gaz sur conduits fumées', 'preceding': true, 'following': false },
    { value: '19', label: 'Poêle charbon', 'preceding': true, 'following': false },
    { value: '20', label: 'Poêle bois', 'preceding': true, 'following': false },
    { value: '21', label: 'Poêle fioul', 'preceding': true, 'following': false },
    { value: '22', label: 'Poêle GPL', 'preceding': true, 'following': false },
    { value: '23', label: "Chaudière gaz installée jusqu'à 1988", 'preceding': true, 'following': false },
    { value: '24', label: "Chaudière fioul installée jusqu'à 1988", 'preceding': true, 'following': false },
    { value: '25', label: "Chaudière gaz sur sol installée jusqu'à 1988 et chgt brûleur", 'preceding': true, 'following': false },
    { value: '26', label: "Chaudière fioul sur sol installée jusqu'à 1988 et chgt brûleur", 'preceding': true, 'following': false },
    { value: '27', label: 'Chaudière gaz installée entre 1989 et 2000', 'preceding': true, 'following': false },
    { value: '28', label: 'Chaudière fioul installée entre 1989 et 2000', 'preceding': true, 'following': false },
    { value: '29', label: 'Chaudière gaz installée à partir de 2001', 'preceding': true, 'following': false },
    { value: '30', label: 'Chaudière fioul installée à partir de 2001', 'preceding': true, 'following': false },
    { value: '31', label: 'Chaudière gaz basse température', 'preceding': true, 'following': false },
    { value: '32', label: 'Chaudière fioul basse température', 'preceding': true, 'following': false },
    { value: '33', label: 'Chaudière gaz condensation', 'preceding': true, 'following': false },
    { value: '34', label: 'Chaudière fioul condensation', 'preceding': true, 'following': false },
    { value: '35', label: 'Chaudière bois classe inconnue', 'preceding': true, 'following': false },
    { value: '36', label: 'Chaudière bois classe 1', 'preceding': true, 'following': false },
    { value: '37', label: 'Chaudière bois classe 2', 'preceding': true, 'following': false },
    { value: '38', label: 'Chaudière bois classe 3', 'preceding': true, 'following': false },
    { value: '39', label: 'Chaudière charbon', 'preceding': true, 'following': false },
    { value: '40', label: 'Réseau de chaleur', 'preceding': true, 'following': false },
    { value: '41', label: 'Chaudière électrique', 'preceding': true, 'following': false },
    { value: '42', label: 'PAC air/air', 'preceding': true, 'following': false },
    { value: '43', label: 'PAC air/eau', 'preceding': true, 'following': false },
    { value: '44', label: 'PAC eau/eau', 'preceding': true, 'following': false },
    {
      value: '45', label: 'PAC géothermique', 'preceding': true, 'following': false
    }
  ]
}

export default {
  heatings: convertFormOptionsToEnum(HeatingsFormOptions.heatings)
}
