export default {
  roles: [
    {
      label: 'Administrateur',
      value: 'Admin'
    },
    {
      label: 'Administrateur territorial',
      value: 'Admin territory'
    },
    {
      label: 'Utilisateur territorial',
      value: 'User territory'
    },
    {
      label: 'Auditeur',
      value: 'Auditor'
    },
    {
      label: 'Particulier',
      value: 'User'
    }
  ]
}
