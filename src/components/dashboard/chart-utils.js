export function dateFromTimestamp (timestamp) {
  let date = new Date(parseInt(timestamp))
  return date.getDate() + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('' + date.getFullYear()).slice(-2)
}

export function getLatestRenovationPlan (plans) {
  return plans.reduce((acc, currentPlan) => {
    return acc.id > currentPlan.id ? acc : currentPlan
  }, {})
}

export function dateWithoutTime (date) {
  return (new Date(date)).setHours(0, 0, 0, 0)
}

export function addDays (date, days) {
  return new Date(date.setDate(date.getDate() + days))
}

export function addWeeks (date, weeks) {
  return new Date(date.setDate(date.getDate() + weeks * 7))
}

export function addMonths (date, months) {
  return new Date(date.setMonth(date.getMonth() + months))
}

export function translateImprovementName (improvementName) {
  switch (improvementName) {
    case 'walls':
      return 'Murs'
    case 'floor':
      return 'Plancher Bas'
    case 'roof':
      return 'Toitures & Combles'
    case 'vents':
      return 'Portes & Fenêtres'
    case 'heating':
      return 'Chauffage'
    case 'ventilation':
      return 'Ventilation'
    case 'thermostat':
      return 'Thermostat'
    case 'dhw':
      return 'ECS'
    case 'airtightness':
      return 'Infiltrations d\'air'
    default:
      return 'N/A'
  }
}

export function translateInsulationType (insulationType) {
  switch (insulationType) {
    case 'ITI':
      return 'ITI'
    case 'ETI':
      return 'ITE'
    default:
      return 'N/A'
  }
}

export function translateAirtighness (airtightness) {
  switch (airtightness) {
    case 'VERY_GOOD':
      return 'Très bonne'
    case 'GOOD':
      return 'Bonne'
    case 'AVERAGE':
      return 'Moyenne'
    case 'BAD':
      return 'Mauvaise'
    default:
      return 'N/A'
  }
}
