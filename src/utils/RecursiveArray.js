function setRecursive (array, path, value) {
  if (path.length === 0) {
    return value
  } else if (!(array instanceof Object)) {
    if (array === undefined && value === undefined) {
      return undefined
    } else {
      return [setRecursive(null, path.slice(1), value)]
    }
  } else {
    array[path[0]] = setRecursive(array[path[0]], path.slice(1), value)
    return array
  }
}

function getRecursive (array, path) {
  if (path.length === 0) {
    return array
  } else if (!(array instanceof Object)) {
    return undefined
  } else {
    return getRecursive(array[path[0]], path.slice(1))
  }
}

export default class RecursiveArray {
  constructor () {
    this.__array = {}
  }

  get (...args) {
    let val = getRecursive(this.__array, Array.from(arguments))
    if (val instanceof Object) {
      return undefined
    }
    return val
  }

  has (...args) {
    let val = getRecursive(this.__array, Array.from(arguments))
    if (val instanceof Object) {
      return Object.values(val).length > 0
    }
    return !!val
  }

  sub (...args) {
    let sub = getRecursive(this.__array, Array.from(arguments))
    if (sub instanceof Object) {
      return sub
    }
    return undefined
  }

  unset (...args) {
    this.__array = setRecursive(this.__array, Array.from(arguments), undefined)
  }

  update (array) {
    this.__array = array
  }

  reset () {
    this.__array = {}
  }

  toString () {
    if (this.__array instanceof Object) {
      return undefined
    } else {
      return this.__array
    }
  }
}
