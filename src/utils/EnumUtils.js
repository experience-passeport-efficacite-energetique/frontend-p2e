export function convertFormOptionsToEnum (formOptions) {
  const Enum = {}

  formOptions.forEach(option => {
    if (!option.ignoreInEnum) {
      Enum[option.value] = option.label
    }
  })

  return Enum
}
