import { Notify } from 'quasar'

class Notifier {
  install (Vue) {
    Vue.notifier = Vue.prototype.$notifier = this
  }

  ok (message, timeout = 2000) {
    Notify.create({
      message: message,
      type: 'positive',
      timeout: timeout
    })
  }

  info (message, timeout = 2000) {
    Notify.create({
      message: message,
      type: 'info',
      timeout: timeout
    })
  }

  warning (message, timeout = 2000) {
    Notify.create({
      message: message,
      type: 'warning',
      timeout: timeout
    })
  }

  ko (message, timeout = 2000) {
    Notify.create({
      message: message,
      type: 'negative',
      timeout: timeout
    })
  }
}

export default new Notifier()
