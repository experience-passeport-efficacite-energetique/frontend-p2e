import ApiCollection from './ApiCollection'
import ApiAuthentication from './ApiAuthentication'

class Api {
  constructor (domain) {
    this.__domain = domain
    this.__events = {}
    this.on(401, response => {
      this.connection.push(null)
    })
  }

  install (Vue) {
    Vue.api = Vue.prototype.$api = this
    this.$http = Vue.http
    this.users = new ApiCollection('users', this)
    this.leads = new ApiCollection('leads', this)
    this.passports = new ApiCollection('passports', this)
    this.renovationPlans = new ApiCollection('renovationPlans', this)
    this.territories = new ApiCollection('territories', this)
    this.ventilations = new ApiCollection('ventilations', this)
    this.heatings = new ApiCollection('heatings', this)
    this.domesticHotWaters = new ApiCollection('domesticHotWaters', this)
    this.combinatories = new ApiCollection('combinatories', this)
    this.staticPrices = new ApiCollection('staticPrices', this)
    this.vigilancePoints = new ApiCollection('vigilancePoints', this)

    this.connection = new ApiAuthentication(Vue.localStorage)
    this.connection.pull()
  }

  authenticate (username, password) {
    return this.post('auth', {
      username: username,
      password: password
    })
      .then(response => {
        let jwt = response.body.token
        this.connection.push(jwt)
        return this.logged()
      })
      .catch(response => {
        throw response.body
      })
  }

  refresh () {
    return this.post('auth/refresh')
      .then(response => {
        let jwt = response.body.token
        this.connection.push(jwt)
        return this.logged()
      })
      .catch(response => {
        this.connection.push(null)
        throw response.body
      })
  }

  fetchPriceRanges (renovationPlanId) {
    return this.get('renovationPlans/' + renovationPlanId + '/priceRanges')
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response
      })
  }

  fetchDashboardData (params = {}) {
    const queryParams = Object.keys(params).length ? '?' + Object.keys(params).map(key => key + '=' + params[key]).join('&') : ''
    return this.get('leads/dashboard' + queryParams)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response
      })
  }

  sendPlan (renovationPlanId, token) {
    return this.get(`renovationPlans/${renovationPlanId}/send_report?jwt=${token}`)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response
      })
  }

  searchPlaces (query, territoryId = null) {
    let fullquery = 'places?search=' + query
    if (territoryId) {
      fullquery += '&territory=' + territoryId
    }
    return this.get(fullquery)
      .then(response => {
        return response.body
      })
      .catch(response => {
        throw response
      })
  }

  unassignCity (territoryId, cityId) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: this.__domain + '/territories/' + territoryId + '/city-assignment',
      method: 'delete',
      body: { city: { id: cityId } }
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  unassignUser (territoryId, userId) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: this.__domain + '/territories/' + territoryId + '/user-assignment',
      method: 'delete',
      body: { user: { id: userId } }
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  addVigilancePointCondition (vigilancePointId, conditionData) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: `${this.__domain}/vigilancePoints/${vigilancePointId}/addCondition`,
      method: 'post',
      body: conditionData
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  updateVigilancePointCondition (vigilancePointId, conditionId, conditionData) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: `${this.__domain}/vigilancePoints/${vigilancePointId}/updateCondition/${conditionId}`,
      method: 'patch',
      body: conditionData
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  removeVigilancePointCondition (vigilancePointId, conditionId) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: `${this.__domain}/vigilancePoints/${vigilancePointId}/removeCondition/${conditionId}`,
      method: 'delete'
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  computeRpVigilancePoint (renovationPlanId) {
    return this.$http({
      headers: { Authorization: 'Bearer ' + this.connection.jwt },
      url: `${this.__domain}/renovationPlans/${renovationPlanId}/computeVps`,
      method: 'post'
    }).then(response => {
      return response.body
    }).catch(response => {
      throw response
    })
  }

  logout () {
    this.connection.push(null)
  }

  logged () {
    return this.connection.user || false
  }

  token () {
    return this.connection.payload
  }

  granted (roles) {
    if (Array.isArray(roles)) {
      return this.logged() && this.logged().profiles.some(role => roles.includes(role))
    } else {
      return this.logged() && this.logged().profiles.includes(roles)
    }
  }

  on (status, callback) {
    if (!this.__events[status]) {
      this.__events[status] = []
    }
    this.__events[status].push(callback)
  }

  trigger (response) {
    if (this.__events[response.status]) {
      this.__events[response.status].forEach(f => f(response))
    }
  }
}

['get', 'delete', 'head', 'jsonp'].forEach(function (method$$1) {
  Api.prototype[method$$1] = function (url, options$$1) {
    url = this.__domain + '/' + url
    let jwt = this.connection.jwt
    if (jwt) {
      options$$1 = Object.assign(options$$1 || {}, { headers: { Authorization: 'Bearer ' + jwt } })
    }
    return this.$http(Object.assign(options$$1 || {}, { url: url, method: method$$1 }))
      .then(response => {
        this.trigger(response)
        return response
      })
      .catch(response => {
        this.trigger(response)
        throw response
      })
  }
});

['post', 'put', 'patch'].forEach(function (method$$1) {
  Api.prototype[method$$1] = function (url, body, options$$1) {
    url = this.__domain + '/' + url
    let jwt = this.connection.jwt
    if (jwt) {
      options$$1 = Object.assign(options$$1 || {}, { headers: { Authorization: 'Bearer ' + jwt } })
    }
    return this.$http(Object.assign(options$$1 || {}, { url: url, method: method$$1, body: body }))
  }
})

export default Api
