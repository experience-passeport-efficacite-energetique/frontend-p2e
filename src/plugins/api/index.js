import Api from './Api'

export default ({ app, router, Vue }) => {
  Vue.use(new Api(process.env.API))
}
