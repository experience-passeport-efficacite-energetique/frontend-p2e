function load (component) {
  return () => import(`src/components/${component}.vue`)
}

const routes = [
  { path: '', component: load('public/Index') },
  { path: '/login', component: load('public/Login') },
  { path: '/account/activate', component: load('account/Activate') },
  { path: '/account/register', component: load('account/Register') },
  { path: '/account/welcome', component: load('account/Welcome') },
  { path: '/account/recover', component: load('account/Recover') },
  { path: '/account/recover/ok', component: load('account/RecoveryAwaiting') },
  { path: '/account/reset-password', component: load('account/ResetPassword') },
  {
    path: '/admin',
    component: load('layouts/ConnectedLayout'),
    children: [
      {
        path: '',
        component: load('admin/Home')
      },
      {
        path: 'users',
        component: load('admin/Users')
      },
      {
        path: 'user',
        component: load('admin/User')
      },
      {
        path: 'leads',
        component: load('admin/Leads')
      },
      {
        path: 'lead',
        component: load('admin/Lead')
      },
      {
        path: 'passports',
        component: load('admin/Passports')
      },
      {
        path: 'passport',
        component: load('admin/Passport')
      },
      {
        path: 'plans',
        component: load('admin/RenovationPlans')
      },
      {
        path: 'plan',
        component: load('admin/RenovationPlan')
      },
      {
        path: 'combinatories',
        component: load('admin/Combinatories')
      },
      {
        path: 'prices',
        component: load('admin/Prices')
      },
      {
        path: 'profile',
        component: load('admin/Profile')
      },
      {
        path: 'territory',
        component: load('admin/Territory')
      },
      {
        path: 'territories',
        component: load('admin/Territories')
      },
      {
        path: 'vigilancePoint',
        component: load('admin/VigilancePoint')
      },
      {
        path: 'vigilancePoints',
        component: load('admin/VigilancePoints')
      },
      {
        path: 'test',
        component: load('layouts/Error404')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
