FROM nginx
COPY dist/spa-mat/ /usr/share/nginx/html/
LABEL traefik.docker.network=traefik_proxy traefik.enable=true traefik.port=80 traefik.frontend.entryPoints=http,https
