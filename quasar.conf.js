// Configuration for your app

module.exports = function(ctx) {
  return {
    // app plugins (/src/plugins)
    plugins: [
      "vue-localstorage",
      "vue-moment",
      "vue-resource",
      "api",
      "notifier",
      "root-url",
      "select-options"
    ],
    css: ["app.styl"],
    extras: [
      ctx.theme.mat ? "roboto-font" : null,
      "material-icons", // optional, you are not bound to it
      "fontawesome"
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    build: {
      scopeHoisting: true,
      // vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack(cfg) {
        cfg.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /node_modules/
        });
      },
      chainWebpack: config => {
        config.module
          .rule("vue")
          .use("vue-svg-inline-loader")
          .loader("vue-svg-inline-loader")
          .options({
            /* ... */
          });
      },
      env: ctx.dev
        ? {
            // so on dev we'll have
            API: JSON.stringify(
              process.env.API_ENDPOINT
                ? process.env.API_ENDPOINT
                : "http://localhost:8081/api"
            )
            // API: JSON.stringify(process.env.API_ENDPOINT ? process.env.API_ENDPOINT : 'https://release-0-5-9-api.app.passeport-efficacite-energetique.org')
          }
        : {
            // and on build (production):
            API: JSON.stringify(process.env.API_ENDPOINT)
          }
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      components: [
        "QAlert",
        "QAutocomplete",
        "QBtn",
        "QBtnGroup",
        "QCard",
        "QCheckbox",
        "QCardActions",
        "QCardMain",
        "QCardMedia",
        "QCardSeparator",
        "QCardTitle",
        "QChip",
        "QCollapsible",
        "QDatetime",
        "QField",
        "QIcon",
        "QInput",
        "QItemSeparator",
        "QItem",
        "QItemSide",
        "QItemMain",
        "QItemTile",
        "QInnerLoading",
        "QLayout",
        "QLayoutHeader",
        "QLayoutFooter",
        "QList",
        "QListHeader",
        "QModal",
        "QOptionGroup",
        "QPage",
        "QPageContainer",
        "QPopover",
        "QRadio",
        "QRouteTab",
        "QSearch",
        "QSelect",
        "QSlider",
        "QSpinner",
        "QSpinnerDots",
        "QTable",
        "QTableColumns",
        "QTh",
        "QTr",
        "QTd",
        "QTab",
        "QTabPane",
        "QTabs",
        "QToggle",
        "QToolbar",
        "QToolbarTitle",
        "QTooltip"
      ],
      directives: ["Ripple"],
      // Quasar plugins
      plugins: ["Notify", "Dialog"],
      i18n: "fr" // Quasar language
      // iconSet: ctx.theme.mat ? 'material-icons' : 'ionicons'
    },
    // animations: 'all' --- includes all animations
    animations: [],
    ssr: {
      pwa: false
    },
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar-PWA',
        // description: 'Best PWA App in town!',
        display: "standalone",
        orientation: "portrait",
        background_color: "#ffffff",
        theme_color: "#027be3",
        icons: [
          {
            src: "statics/favicon-p2e.png",
            sizes: "64x64",
            type: "image/png"
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
      iosStatusBarPadding: true, // add the dynamic top padding on iOS mobile devices
      backButtonExit: false
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack(cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration
        // appId: 'quasar-app'
      }
    }
  };
};
