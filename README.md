[![pipeline status](https://gitlab.com/experience-passeport-efficacite-energetique/frontend-p2e/badges/dev/pipeline.svg)](https://gitlab.com/experience-passeport-efficacite-energetique/frontend-p2e/commits/dev) [![coverage report](https://gitlab.com/experience-passeport-efficacite-energetique/frontend-p2e/badges/dev/coverage.svg)](https://experience-passeport-efficacite-energetique.gitlab.io/frontend-p2e/lcov-report/)

This is the Frontend part of Passeport Efficacité Energétique application. This was developed as a SPA [Quasar application (v0.17.20)](https://quasar.dev/). Works with Node 8.x.
To view backend application repo -> https://gitlab.com/experience-passeport-efficacite-energetique/backend-p2e

- [Dependencies](#dependencies)
- [Build Setup](#build-setup)
- [Dev](#dev)
- [Tests](#tests)
- [CI Workflow (to be improve!)](#ci-workflow-to-be-improve)
  - [Dev](#dev-1)
  - [Staging](#staging)
  - [Prod](#prod)

## Dependencies

- [VueMoment](https://github.com/brockpetrie/vue-moment) to add date filters for views
- [Vue2Leaflet](https://github.com/brockpetrie/vue-moment) to handle maps displaying
- Idea: Use [Leaflet BAN Geocoder](https://github.com/entrepreneur-interet-general/leaflet-geocoder-ban) or [Node-Ban](https://www.npmjs.com/package/node-ban) for address normalization

## Build Setup

```bash
# install dependencies
$ npm install

# install quasar cli
$ npm install -g quasar-cli

# serve with hot reload at localhost:8080
$ quasar dev

# serve with specific API endpoint
$ API_ENDPOINT="https://release-0-3-1-api.app.passeport-efficacite-energetique.org" quasar dev

# build for production with minification
$ quasar build

# lint code
$ quasar lint
```

## Dev

Create a `docker-back/env.yml` file with your dev config. You can most likely use `docker-back/env.dist.yml`.
If you are using linux, you might have to make your `env.yml` file executable with `chmode +x env.yml`

Lauch the backend :

```
cd ./docker-back
docker-compose up
```

if it is the first launch, add geo data to the db using the php docker:
(warning, the name of the docker might not be the same, you can check the names of your running containers with `docker ps`):

```sh
docker-compose exec web php bin/console db:load etc/db_static_regions.yml
docker-compose exec web php bin/console db:load etc/db_static_departments.yml
#cities can take quite a bit of time, 1 to 5 minutes
docker-compose exec web php bin/console db:load etc/db_static_cities.yml
```

## Tests

We use [Cypress](https://www.cypress.io/) for end-to-end testing.
Create a `cypress.env.json` or `cypress.json` from `cypress.dist.json`

## CI Workflow (to be improve!)

### Dev

During dev commit to whatever branch you want (prefer feature-_ named branch for each feature, then ask for a merge request into dev), except `master` or with tagged ref like `release-_`. The minimal pipeline is to run test and coverage

### Staging

If you want to deploy to staging env, tag with ref like `release-*`. It will run tests like in dev but additionaly deploy to the server see CI/CD > Environments

### Prod

On commit on master, it will run all the pipe and deploy for production on the server
